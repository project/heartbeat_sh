<?php

namespace Drupal\heartbeat_sh\Form;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\heartbeat_sh\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @var TypedConfigManagerInterface
   */
  protected TypedConfigManagerInterface $typedConfig;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->typedConfig = $container->get('config.typed');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['heartbeat_sh.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  private function getMapping() {
    $mapping = [];
    $plugin_id = $this->getEditableConfigNames()[0];
    $schema = $this->typedConfig->getDefinition($plugin_id);
    foreach ($schema['mapping'] as $key => $definition) {
      if (in_array($key, ['langcode', '_core'])) {
        continue;
      }
      $mapping[$key] = $definition;
    }
    return $mapping;
  }

  private function getConfig() {
    return $this->config($this->getEditableConfigNames()[0]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['actions']['#type'] = 'actions';

    foreach ($this->getMapping() as $key => $definition) {
      if ($definition['type'] == 'string') {
        $form[$key] = [
          '#type' => 'textfield',
          '#title' => $this->t($definition['title']),
          '#default_value' => $config->get($key),
          '#description' => $this->t($definition['label']),
        ];
      } elseif ($definition['type'] == 'boolean') {
        $form[$key] = [
          '#type' => 'checkbox',
          '#title' => $this->t($definition['title']),
          '#default_value' => $config->get($key),
          '#description' => $this->t($definition['label']),
        ];
      }
      // else { Not implemented. }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    foreach ($this->getMapping() as $key => $definition) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
